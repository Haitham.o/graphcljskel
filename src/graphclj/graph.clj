(ns graphclj.graph
  (:require [clojure.string :as str]))

;; Generate a graph from the lines
(defn gen-graph [lines]
    "Returns a hashmap contating the graph"
    (loop [map {} l lines]
      (if (seq l)
        (let [e (clojure.string/split (first l) #" ") node1 (Integer. (first e)) node2 (Integer. (second e))]
          (if (contains? map node1)
            (if (contains? map node2)
              (recur (assoc-in (assoc-in map [node2 :neigh] (conj (get-in map [node2 :neigh]) node1)) [node1 :neigh] (conj (get-in map [node1 :neigh]) node2)) (rest l))
              (recur (assoc-in (assoc map node2 {:neigh #{node1}}) [node1 :neigh] (conj (get-in map [node1 :neigh]) node2)) (rest l)))
            (if (contains? map node2)
              (recur (assoc-in (assoc map node1 {:neigh #{node2}}) [node2 :neigh] (conj (get-in map [node2 :neigh]) node1)) (rest l))
              (recur (assoc (assoc map node1 {:neigh #{node2}}) node2 {:neigh #{node1}}) (rest l))))
        )
        map
      )
    )
)


(defn erdos-renyi-rnd [n,p]
  "Returns a G_{n,p} random graph, also known as an Erdős-Rényi graph"
  (loop [i 0 j 0 res []]
    (if (< i n)
      (if (< j n)
	(if (not= i j)	
	  (let [x (rand-int 100)]
	    (if (< x (* p 100))
	      (recur i (inc j) (conj res (str i " " j)))
	      (recur i (inc j) res)))
	  (recur i (inc j) res))
	(recur (inc i) (inc i) res))
      (let [graph (gen-graph res)]
		(loop [i 0 g graph]
		  (if (< i n)
			(if (seq (get (get g i) :neigh))
			  (recur (inc i) g)
			  (recur (inc i) (assoc g i (assoc (get g i) :neigh #{}))))
			g
		  )
		)
      )
    )
  )
)


