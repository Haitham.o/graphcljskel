(ns graphclj.centrality
    (:require [graphclj.graph :as graph]
              [clojure.set :as set]))



(defn degrees [g]
  "Calculates the degree centrality for each node"
  (loop [ng {} ag g]
    (if(seq ag)
      (recur (assoc ng (first (first ag)) (assoc (second (first ag)) :degree (count (get (second (first ag)) :neigh)))) (rest ag))
      ng
    )
  )
)


(defn getallnoeud [g e]
  (loop[ne e s #{}]
    (if(seq ne)
      (recur (rest ne) (set/union s (get (get g (first ne)) :neigh)))
      s
    )
  )
)


(defn addnoeud [m l t]
  (loop[m m nl l]
    (if(seq nl)
      (if(contains? m (first nl))
        (recur m  (rest nl))
        (recur (assoc m (first nl) t) (rest nl)))
      m
    )
  )
)


(defn distance [g n]
  "Calculate the distances of one node to all the others"
  (loop[m (assoc {} n 0.0), t 1.0, e (set (list n))]
    (if(< (count e) (count g))
      (let[l (getallnoeud g e)]
        (recur (addnoeud m l t) (+ t 1) (set/union e l)))
      m
    )
  )
)


(defn closeness [g n]
  "Returns the closeness for node n in graph g"
  [g n]
  (reduce + 0.0 (map (fn [x] (if (= x 0.0) x (/ 1 x))) (vals (distance g n))))
)


(defn closeness-all [g]
  "Returns the closeness for all nodes in graph g"
  (let [key (keys g)]
    (loop [key key,res g]
      (if (seq key)
        (recur (rest key) (assoc res (first key) (assoc (get res (first key)) :close (closeness g (first key)))))
        res
      )
    )
  )
)

